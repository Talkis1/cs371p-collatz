// -----------
// Collatz.cpp
// -----------

// --------
// includes
// --------

#include <cassert> // assert
#include <iostream>
#include "Collatz.hpp"

// ------------
// collatz_eval
// ------------

tuple_type_2 collatz_eval (const tuple_type_1& t1) {
	auto [i, j] = t1;
    // int count=0;
    assert(i > 0);
    assert(j > 0);
    int v=0;
    // std::vector<int> myVector;
    for (int n = i; n <= j; ++n) {
        int num = n;
        int count = 1;
        while (num!=1){
            if ((num%2) != 0){
                num=(3*num)+1;
            }
            else{
                num=(num/2);

            }
            ++count;
        }
        if (n==1){
            count=1;
        }
        if (count>v){
            v=count;
        }
        // Add your computation steps here
        // cout<<"here is the count for this number: "<<v<<endl<<endl;
    }
    // int v = i + j; // fix!
    assert(v > 0);
    return {i, j, v};}
