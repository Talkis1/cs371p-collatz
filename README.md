# CS371p: Object-Oriented Programming Collatz Repo

* Name: Tristan Alkis

* EID: tma886

* GitLab ID: Talkis1

* HackerRank ID: (your HackerRank ID)

* Git SHA: (most recent Git SHA, final change to your repo will change this, that's ok)

* GitLab Pipelines: (link to your GitLab CI Pipeline)

* Estimated completion time: (estimated time in hours, int or float)

* Actual completion time: (actual time in hours, int or float)

* Comments: (any additional comments you have)
